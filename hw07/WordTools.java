// CSE 002 HW07
// George Huang
// 10/30/2018
// This program will help user do things 
import java.util.*;
public class WordTools{
  public static void main(String arg[]){
    String text = " "; 
    Scanner myScan = new Scanner(System.in);
    text = sampleText(); 
    System.out.println("you entered: " + text); 
    String menu = " "; 
    menu = printMenu(); 
    while(!menu.equals("q")){
      if(menu.equals("c")){
        getNumOfNonWSCharacters(text); 
      }
      else if(menu.equals("w")){
        getNumOfWords(text); 
    }
      else if(menu.equals("f")){
        findText(text); 
      }
      else if(menu.equals("r")){
        replaceExclamation(text); 
      }
      else if(menu.equals("s")){
        System.out.println("Edited text: " + removeSpace(text));
      }
      menu = printMenu(); 
    }
    
  }//end of the main method 
  public static String sampleText(){
    System.out.println("Enter a sample text "); 
    Scanner myScan = new Scanner(System.in);
    String userInput = myScan.nextLine(); 
     
    return userInput; 
    
  } //end of sample text method 
  public static String printMenu(){
    boolean correctInput = false; 
    String userInput = " "; 
    System.out.println("Menu"); 
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    Scanner myScan = new Scanner(System.in);
    System.out.println("Choose an option: ");
    while(!correctInput){
    userInput = myScan.nextLine(); 
    if(userInput.equals("c") || userInput.equals("w") || userInput.equals("f") || 
       userInput.equals("r") || userInput.equals("s") || userInput.equals("q") )
    {
      correctInput = true; 
    }
    else System.out.println("Please enter again: "); 
    }
    return userInput; 
  }//end of printMenu method
  public static void getNumOfNonWSCharacters(String text){
    int spaces = 0; 
    int i = 0; 
    char a = text.charAt(i);
    int b = text.length();
    int letters = 0; 
    /*for(int i, i < (b-1), i++)
    {
      if(a == ' '){
        spaces++; 
      }
      
    }Do not understand why it cannot work
    */
    while(i < (b - 1))
    {
      if(a == ' ')
      {
        spaces++; 
      }
      i++; 
    }
    letters = b - spaces; 
    System.out.println("Number of non-whitespace characters: " + letters); 
  }
  public static void getNumOfWords(String text) {
    text = removeSpace(text); 
    int i = 0; 
    char a = text.charAt(i);
    int b = text.length();
    int words = 1; 
    while(i < (b - 1))
    {
      if(a == ' ')
      {
        words++; 
      }
      i++; 
    }
    System.out.println("Number of words: " + words); 
  }
  public static String removeSpace(String text){
    text = text.replaceAll("\\s+"," "); 
    return text; 
  }
  public static void findText(String text){
    String userInput = " "; 
    Scanner myScan = new Scanner(System.in);
    System.out.println("Enter a word or phrase to be found: ");
    userInput = myScan.nextLine(); 
    int i = 0; 
    char a = text.charAt(i);
    int b = text.length();
    int counter = 0; 
    text = text.replaceAll("0", "1"); 
    text = text.replaceAll(userInput, "0"); 
    
    while(i < (b - 1))
    {
      if( a == '0')
      {
        counter++; 
      }
      i++; 
    }
    System.out.println("\"" + userInput + "\" instances: " + counter); 
    
  }//Do not know why it does not work
  
  public static void replaceExclamation(String text){
    text = text.replaceAll("!", "."); 
    System.out.println("Edited text: " + text); 
  }
}//end of class