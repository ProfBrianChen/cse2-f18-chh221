// CSE 002 HW02
// George Huang
// 09/09/2018
public class Arithmetic{
  public static void main(String arg[]){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;

    //calculate total cost of each item
    double pantsCost = pantsPrice * numPants; 
    double shirtCost = shirtPrice * numShirts; 
    double beltPrice = beltCost * numBelts; //note it is beltPrice 
    
    
    //each item's tax
    double pantsTax = pantsCost * paSalesTax;
    double shirtTax = shirtCost * paSalesTax;
    double beltTax = beltPrice * paSalesTax;
    
    //total cost before tax 
    double totalCost = pantsCost + shirtCost + beltPrice; 
    
    //total sales tax
    double totalTax = pantsTax + shirtTax + beltTax; 
    
    //total paid 
    double totalReal = totalCost + totalTax; 
    
    System.out.println("The total is : " + totalReal);
    
    
  
    
    
    
    
  } 
}