// CSE 002 lab09
// George Huang
// 11/15/2018
// This program 
//import java.util.*;
public class MethodsArrays{
  public static int[] copy(int array[])
  {
    int [] b = new int[8]; 
    for(int i = 0; i < 8; i++)
    {
      b[i] = array[i]; 
    }
    return b; 
  }
  public static int[] inverter(int array[])
  {
    int size = 8; 
    int [] c = new int[size];
    for(int i = 0; i < size; i++)
    {
      c[i] = array[size - 1 - i]; 
      
    }
    return c; 
  }
  public static int[] inverter2(int[] a)
  {
    int b[] = copy(a); 
    b = inverter(b); 
    
    return b; 
  }
  
  public static void print(int[] a)
  {
    for(int i = 0; i < a.length; i++)
    {
      System.out.print(a[i]+ ", ");
    }
  }
  
  public static void main(String arg[]){
    int[] array0 = {1, 2, 3, 4, 5, 6, 7, 8};
    int[] array1 = {2, 4, 6, 8, 9, 11, 13, 15};
    int[] array2 = {8, 7, 6, 5, 4, 3, 2, 1};
    int[] array3 = new int[array0.length]; 
    
   
    print(inverter(array0)); // should be inverted
    System.out.println();
    print(inverter2(array1)); // should remain unchanged 
    System.out.println();
    array3 = inverter2(array2);//reversed copy of array2 
    print(array3);
    System.out.println();
      
      
      
  }//end of main method
  
  
}//end of class

