// CSE 002 HW06
// George Huang
// 10/23/2018
// This program will print an X. 
import java.util.*;
public class EncryptedX{
  public static void main(String arg[]){
    
    int inputNumber = 0; 
    Scanner myScan = new Scanner(System.in);
    System.out.println("Please enter an integer between 0-100: "); //Trying to get the correct user input 
    
    if(myScan.hasNextInt())
    {
      inputNumber = myScan.nextInt(); 
    }
    else 
    {
      do 
      {
        System.out.println("Error, Please enter an integer between 0-100: ");
        inputNumber = myScan.nextInt(); 
        if(inputNumber < 0 || inputNumber > 100)
        {
          continue; 
        }
      }
      while(!myScan.hasNextInt()); 
    }
    
    for(int numRows = 0; numRows <= inputNumber; numRows++) 
    {
      for(int numColumn = 0; numColumn <= inputNumber; numColumn++) 
      {
       
        if((numRows == numColumn) || ((inputNumber - numRows) == numColumn)) 
        {
          System.out.print(" ");
        }
        else
        {
          System.out.print("*");
        }
        
      }
      System.out.println(); 
        
    
    }
  }
}