// CSE 002 hw08
// George Huang
// 11/14/2018
// This program 
//import java.util.*;
import java.util.Scanner;
public class Shuffling{ 
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    for(int i=0; i<52; i++)
    { 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
      System.out.print(cards[i]+" "); 
    } 
    System.out.println();
    printArray(cards); 
    shuffle(cards); 
    System.out.println("shuffled"); 
    printArray(cards); 
     
    
    while(again == 1)
    { 
       hand = getHand(cards,index,numCards); 
       System.out.println("hand");
       printArray(hand);
       
       index = index - numCards; 
       System.out.println("Enter a 1 if you want another hand drawn"); 
       again = scan.nextInt(); 
    }
  }
  public static void printArray(String[] cards)
  {
    
    for(int i = 0; i < cards.length; i++)
    {
      System.out.print(cards[i] + " "); 
    }
    System.out.println(); 
  }
  public static String[] shuffle(String[] cards)
  {
    for(int i = 0; i < 52; i++)
    {
      int number = (int)(Math.random() * 51);  
      String middle = cards[number]; 
      cards[number] = cards[i]; 
      cards[i] = middle; 
    }
    return cards; 
  }
  public static String[] getHand(String[] cards, int index, int numCards)
  {
    String[] hand = new String[numCards]; 
    for(int i = 0; i < numCards; i++)
    {
      hand[i] = cards[index - i]; 
      System.out.print(hand[i] + " "); 
    }
    return hand; 
  }
    
    
    
    
    
    
    
    
    
    
}
