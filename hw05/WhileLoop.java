// CSE 002 HW05
// George Huang
// 10/9/2018
// This program 
import java.util.*;
public class WhileLoop{
  public static void main(String arg[]){
    int onePairCounter = 0; 
    int a = 0; //card1
    int b = 0; //card2
    int c = 0; //card3
    int d = 0; //card4
    int e = 0; //card5
    int numHands = 0;
    int fourKind = 0; 
    double fourProb = 0.00; 
    int threeKind = 0; 
    double threeProb = 0.00; 
    int twoPair = 0; 
    double twoProb = 0.00; 
    int onePair = 0; 
    double oneProb = 0.00; 
    int counter = 0; 
    Scanner myScan = new Scanner(System.in); 
    
    System.out.println("How many times generate hands?  "); 
    while(!myScan.hasNextInt())
    {
      myScan.next(); 
      System.out.println("Error, Please enter an integer: ");
    }
    numHands = myScan.nextInt(); 
    
    for(int i = 1; i <= numHands; i++)
    {
      a = (int)(Math.random()*(52)) + 1;
      b = (int)(Math.random()*(52)) + 1; 
      c = (int)(Math.random()*(52)) + 1; 
      d = (int)(Math.random()*(52)) + 1; 
      e = (int)(Math.random()*(52)) + 1; 
      
      while(a == b || a == c || a == d 
              || a == e || b == c || b == d 
           || b == e || c == d || c == d
           || d == e)
      {
        a = (int)(Math.random()*(52)) + 1; 
        b = (int)(Math.random()*(52)) + 1; 
        c = (int)(Math.random()*(52)) + 1; 
        d = (int)(Math.random()*(52)) + 1; 
        e = (int)(Math.random()*(52)) + 1;
      }
      
      a = a % 13; 
      b = b % 13; 
      c = c % 13; 
      d = d % 13; 
      e = e % 13; 
      //Four of a kind
      if((a == b && a == c && a == d) || (a == b && a == c && a == e) || 
         (a == b && a == d && a == e) || (a == c && a == d && a == e) || 
         (b == c && b == d && b == e))
      {
        fourKind++; 
      }
      fourKind = fourKind / numHands; 
      //For three of a kind 
      if((c == d && c == e) || (b == d && b == e) || (b == c && b == e) || 
         (b == c && b == d) || (a == d && a == e) || (a == c && a == e) || 
         (a == c && a == d) || (a == b && a == e) || (a == b && a == d) ||
         (a == b && a == c))
           {
        threeKind++; 
      }
      
      if((a == b && c == d) || (a == c && b == d) || (a == d && b == c) ||
         (a == b && c == e) || (a == c && b == e) || (a == e && c == d) || 
         (a == b && d == e) || (a == d && b == e) || (a == e && b == e) || 
         (a == c && d == d) || (a == d && c == e) || (a == e && c == d) || 
         (b == c && d == e) || (b == d && c == e) || (b == e && c == d))
           {
        twoPair++; 
      }
      //if((a == b && a != c && a != d && a != e
      if((a == b) || (a == c) || (a == d) || (a == e) || (b == c) || (b == d) || 
         (b == e) || (c == d) || (c == e) || (d == e))
      {
        counter++; //counter to determine two pairs, three of a kind, four of a kind
        if(counter == 6) //four of a kind
        {
          counter = counter - 6; //I do not want to delete my previous hard work, 
                                 //so I'll neglect things other than a pair. 
        }
        if(counter == 3) //Three of a kind
        {
          counter = counter - 3; 
        }
        if(counter == 2) //Two Pairs
        {
          counter = counter - 2; 
        }
        if(counter == 1)
        {
          onePair++; 
        }
        counter = 0; 
      }
      
    }
    //Assigning values 
    fourProb = fourKind / (double) numHands; 
    threeProb = threeKind / (double)numHands; 
    twoProb = twoPair / (double)numHands;
    oneProb = onePair / (double)numHands;
    
    
    System.out.println("The number of loops: " + numHands); 
    System.out.printf("The probability of Four-of-a-kind: %1.3f \n", fourProb); 
    System.out.printf("The probability of Three-of-a-kind: %1.3f \n", threeProb);
    System.out.printf("The probability of Two Pairs: %1.3f \n", twoProb);
    System.out.printf("The probability of one pair: %1.3f \n", oneProb); 
    
    
  }
}