// CSE 002 Lab02
// George Huang
// 09/06/2018
//My bicycle cyclometer (meant to measure speed, distance, etc.) records two kinds of data, the time elapsed in seconds,
//and the number of rotations of the front wheel during that time. For two trips, given time and rotation count, your program should

public class Cyclometer{
  
  public static void main(String arg[]){
	// our input data. 

	int secsTrip1=480;  //
  int secsTrip2=3220;  //
	int countsTrip1=1561;  //
	double countsTrip2=9037; //
  // our intermediate variables and output data. Document!
  double wheelDiameter=27.0;  //
  double PI=3.14159; //
  int feetPerMile=5280;  //
  int inchesPerFoot=12;   //
  double secondsPerMinute=60;  //
	double distanceTrip1, distanceTrip2,totalDistance;  //
  
    
  //printing intput data
             System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
  //________________________________________________
	distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2;  
  
    	//Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");
  
    
  }//end of the main method
}//end of the class