// CSE 002 HW04
// George Huang
// 09/23/2018
// This program will give out the combination of two dices with slangs
import java.util.*;
public class CrapsSwitch{
  public static void main(String arg[]){
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Press 0 if you would like randomly cast dice, and press 1 if you would like to state the two dice they want to evaluate.");
    int userAnswer = myScanner.nextInt();
    
    int firstDice = 0; 
    int secondDice = 0; 
    int firstNumber = 0; 
    int secondNumber = 0; 
    
    
    switch(userAnswer) // randomly cast dice 
    {
      case 0: 
      firstDice = (int)(Math.random()*5)+1; 
      secondDice = (int)(Math.random()*5)+1;
        break; 
      case 1: 
        System.out.print("Please enter the integer for the 1st dice (enter number from 1-6): ");
        firstDice = myScanner.nextInt();
        while(firstDice < 1 || firstDice > 6 ) // making sure the number is in range 
        {
          System.out.print("Error, Please enter a number from 1-6: ");
          firstDice = myScanner.nextInt();
        }
        //end of 1st dice 
        System.out.print("Please enter the integer for the 2nd dice (enter number from 1-6): ");
        secondDice = myScanner.nextInt();
        while(secondDice < 1 || secondDice > 6 )
        {
          System.out.print("Error, Please enter a number from 1-6: ");
          secondDice = myScanner.nextInt();
        }
    }
   
    
/*    
    switch(firstDice < secondDice)
    {
      case true:
        firstNumber = firstDice; 
        secondNumber = secondDice;
        break;
    }
    this prob will not work 
*/
    System.out.println("The first roll is " + firstDice + " " + "and the second roll is : " + secondDice);  
    //Comparing two numbers to save time 
    if(firstDice < secondDice)
    {
      firstNumber = firstDice; 
      secondNumber = secondDice; 
    } // I don't know how to use switch for this 
    
    if(secondDice < firstDice)
    {
      firstNumber = secondDice; 
      secondNumber = firstDice; 
    }
    if(firstDice == secondDice)
    {
      firstNumber = firstDice; 
      secondNumber = secondDice; 
    }
    //Printing out the results 
    switch(firstNumber) // big switch
    {
      case 1: 
        switch(secondNumber)
        {
          case 1: System.out.println("Snake Eyes");
            break;
          case 2: System.out.println("Ace Deuce");
            break; 
          case 3: System.out.println("Easy Four");
            break;
          case 4: System.out.println("Fever Five");
            break;
          case 5: System.out.println("Easy Six");
            break;
          case 6: System.out.println("Seven out");
            break;    
        }
        break;
      case 2: //when the first number is equal to 2...
        switch(secondNumber)
        {
          case 2: System.out.println("Hard four");
            break;
          case 3: System.out.println("Fever five");
            break;
          case 4: System.out.println("Easy six");
            break;
          case 5: System.out.println("Seven out");
            break;
          case 6: System.out.println("Easy Eight");
            break;
        }
        break; 
      case 3: 
        switch(secondNumber)
        {
          case 3: System.out.println("Hard six");
            break;
          case 4: System.out.println("Seven out");
            break;
          case 5: System.out.println("Easy Eight");
            break;
          case 6: System.out.println("Nine");
            break;
        }
        break; 
      case 4: 
        switch(secondNumber)
        {
          case 4: System.out.println("Hard Eight");
            break;
          case 5: System.out.println("Nine");
            break;
          case 6: System.out.println("Easy Ten");
            break;
        }
        break; 
      case 5: 
        switch(secondNumber)
        {
          case 5: System.out.println("Hard Ten");
            break;
          case 6: System.out.println("Yo-leven");
            break;
        }
      case 6: 
        switch(secondNumber)
        {
          case 6: System.out.println("Boxcars");
        }
    }//close the big switch statement
    
    
  }
}