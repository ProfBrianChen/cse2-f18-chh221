// CSE 002 HW04
// George Huang
// 09/23/2018
// This program will give out the combination of two dices with slangs
import java.util.*;
public class CrapsIf{
  public static void main(String arg[]){
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Press 0 if you would like randomly cast dice, and press 1 if you would like to state the two dice they want to evaluate.");
    int userAnswer = myScanner.nextInt();
    
    int firstDice = 0; 
    int secondDice = 0; 
    int firstNumber = 0; 
    int secondNumber = 0; 
    
    
    if (userAnswer == 0) // randomly cast dice 
    {
      firstDice = (int)(Math.random()*5)+1; 
      secondDice = (int)(Math.random()*5)+1;
    }
    if (userAnswer == 1) //user provide dice
    {
      System.out.print("Please enter the integer for the 1st dice (enter number from 1-6): ");
      firstDice = myScanner.nextInt();
      while(firstDice < 1 || firstDice > 6 )
      {
        System.out.print("Error, Please enter a number from 1-6: ");
        firstDice = myScanner.nextInt();
      }
      //end of 1st dice 
      System.out.print("Please enter the integer for the 2nd dice (enter number from 1-6): ");
      secondDice = myScanner.nextInt();
      while(secondDice < 1 || secondDice > 6 )
      {
        System.out.print("Error, Please enter a number from 1-6: ");
        secondDice = myScanner.nextInt();
      }
    }
    System.out.println("The first roll is " + firstDice + " " + "and the second roll is : " + secondDice); 
    
    //Comparing two numbers to save time 
    if(firstDice < secondDice)
    {
      firstNumber = firstDice; 
      secondNumber = secondDice; 
    }
    
    if(secondDice < firstDice)
    {
      firstNumber = secondDice; 
      secondNumber = firstDice; 
    }
    if(firstDice == secondDice)
    {
      firstNumber = firstDice; 
      secondNumber = secondDice; 
    }
    //Printing out the results 
    if(firstNumber == 1)
    {
      if(secondNumber == 1) 
      {
        System.out.println("Snake Eyes");
      }
      if(secondNumber == 2) 
      {
        System.out.println("Ace Deuce");
      }
      if(secondNumber == 3) 
      {
        System.out.println("Easy Four");
      }
      if(secondNumber == 4) 
      {
        System.out.println("Fever Five");
      }
      if(secondNumber == 5) 
      {
        System.out.println("Easy Six");
      }
      if(secondNumber == 6) 
      {
        System.out.println("Seven out");
      }
    }
    if(firstNumber == 2)
    {
      if(secondNumber == 2) 
      {
        System.out.println("Hard four");
      }
      if(secondNumber == 3) 
      {
        System.out.println("Fever five");
      }
      if(secondNumber == 4) 
      {
        System.out.println("Easy six");
      }
      if(secondNumber == 5) 
      {
        System.out.println("Seven out");
      }
      if(secondNumber == 6) 
      {
        System.out.println("Easy Eight");
      }
    }
    if(firstNumber == 3)
    {
      if(secondNumber == 3) 
      {
        System.out.println("Hard six");
      }
      if(secondNumber == 4) 
      {
        System.out.println("Seven out");
      }
      if(secondNumber == 5) 
      {
        System.out.println("Easy Eight");
      }
      if(secondNumber == 6) 
      {
        System.out.println("Nine");
      }
    }
    if(firstNumber == 4)
    {
      if(secondNumber == 4) 
      {
        System.out.println("Hard Eight");
      }
      if(secondNumber == 5) 
      {
        System.out.println("Nine");
      }
      if(secondNumber == 6) 
      {
        System.out.println("Easy Ten");
      }
    }
    if(firstNumber == 5)
    {
      if(secondNumber == 5) 
      {
        System.out.println("Hard Ten");
      }
      if(secondNumber == 4) 
      {
        System.out.println("Yo-leven");
      }
    }
    if(firstNumber == 6)
    {
      if(secondNumber == 6) 
      {
        System.out.println("Boxcars");
      }
    }
      
      
      
      
      
    
  }
}