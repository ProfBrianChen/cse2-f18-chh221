// CSE 002 lab06
// George Huang
// 10/14/2018 
// This program print stuff
import java.util.*;
public class PatternD{
  public static void main(String arg[]){
    int inputNumber = 0; 
    
    Scanner myScan = new Scanner(System.in);
    System.out.println("Please enter an integer between 1-10: "); 
    
    if(myScan.hasNextInt())
    {
      inputNumber = myScan.nextInt(); 
    }
    else 
    {
      do 
      {
        System.out.println("Error, Please enter an integer between 1-10: ");
        inputNumber = myScan.nextInt(); 
        if(inputNumber < 1 || inputNumber > 10)
        {
          continue; 
        }
      }
      while(!myScan.hasNextInt()); 
    }
    
    
    
    
    for(int numRows = 0; numRows <= inputNumber; numRows++)
    {
      for(int i = (inputNumber - numRows); i > 0; i-- )
      {
        System.out.print(i + " "); 
        
      }
      System.out.println(); 
    }
    
    
  }
}