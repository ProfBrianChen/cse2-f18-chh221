// CSE 002 HW03
// George Huang
// 09/18/2018
// This program asks user for the dimensions of a pyramid and returns the volume inside the pyramid
import java.util.*;
public class Pyramid{
  public static void main(String arg[]){
    Scanner myScan = new Scanner( System.in );
    System.out.print("The square side of the pyramid is (input length): ");
    double sideLength = myScan.nextDouble();
    System.out.print("The height of the pyramid is (input height): " );
    double heightLength = myScan.nextDouble();
    double pyramidVolume = sideLength * sideLength * heightLength / 3; 
    System.out.println("The volume inside the pyramid is: " + pyramidVolume );
    
    
    
    
    
  }
}
