// CSE 002 HW03
// George Huang
// 09/18/2018
//This program will ask the user for the number of acres of land affected by hurricane precipitation and how many inches of rain were dropped on average.
import java.util.*;
public class Convert{
  public static void main(String arg[]){
    Scanner scanRain = new Scanner( System.in );
    System.out.print("Enter the affected area in acres: ");
    double areaAcres = scanRain.nextDouble();
    System.out.print("Enter the rainfall in the affected area: " );
    double rainFall = scanRain.nextDouble();
    double areaInches = areaAcres * 6272640; 
    
    double gallonsNumber = (areaInches * rainFall) / 231; 
    double cubicMiles = gallonsNumber * 0.000000000000908169;
    System.out.println(cubicMiles + " cubic miles. ");
    
    
    
    
    
  }
}
