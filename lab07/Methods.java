// CSE 002 lab07
// George Huang
// 10/25/2018
// This program prints stuff

import java.util.Random;
public class Methods{
  public static void main(String arg[]){
    
    //Random randomGenerator = new Random();
    first(); 
    action(); 
    conclusion(); 
    
  }//main method
  public static String first()
  {
    String adj = Adj(); 
    String sNoun = subjectNoun(); 
    String verb = verbs(); 
    String oNoun = objectNoun(); 
    System.out.println("The " + adj + adj + sNoun + verb + "the " + adj + oNoun); 
    return("whatsup"); 
  }
  public static String action()
  {
    String adj = Adj(); 
    String sNoun = subjectNoun(); 
    String verb = verbs(); 
    String oNoun = objectNoun(); 
    System.out.println(sNoun + "used " + oNoun + "to " + verb + oNoun + "at the " + adj + sNoun); 
    return("whatsup"); 
    
  }
  public static String conclusion()
  {
    String adj = Adj(); 
    String sNoun = subjectNoun(); 
    String verb = verbs(); 
    String oNoun = objectNoun(); 
    System.out.println("That " + sNoun + verb + "her " + oNoun); 
    return("whatsup"); 
  }
  public static String Adj(){
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String adj = " "; 
  
  
    switch(randomInt)
    {
      case 0: 
      adj = "hilarious ";
        break; 
      case 1: 
        adj = "flaming "; 
        break; 
      case 2: 
        adj = "happy ";
        break; 
      case 3: 
        adj = "hairless ";
        break; 
      case 4: 
        adj = "blue ";
        break;
      case 5: 
        adj = "talking ";
        break;
      case 6: 
        adj = "angry ";
        break;
      case 7: 
        adj = "magical ";
        break;
      case 8: 
        adj = "rebellious ";
        break;
      case 9: 
        adj = "greedy ";
        break;
    }//end of the switch statement
    return adj; 
  }//end of the adj method
  public static String subjectNoun()
  {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String sNoun = " "; 
  
  
    switch(randomInt)
    {
      case 0: 
      sNoun = "gramma ";
        break; 
      case 1: 
       sNoun = "torch "; 
        break; 
      case 2: 
       sNoun = "book ";
        break; 
      case 3: 
        sNoun = "shampoo ";
        break; 
      case 4: 
        sNoun = "Rick ";
        break;
      case 5: 
        sNoun = "Stevie ";
        break;
      case 6: 
        sNoun = "cookie ";
        break;
      case 7: 
        sNoun = "magical ";
        break;
      case 8: 
        sNoun = "table ";
        break;
      case 9: 
        sNoun = "charger ";
        break; 
    }
    return sNoun; 
  }//end of subjective noun method
  public static String verbs()
  {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String verb = " "; 
    switch(randomInt)
    {
      case 0: 
      verb = "agreed ";
        break; 
      case 1: 
       verb = "hijacked "; 
        break; 
      case 2: 
       verb = "bore ";
        break; 
      case 3: 
        verb = "broke ";
        break; 
      case 4: 
        verb = "rode ";
        break;
      case 5: 
        verb = "read ";
        break;
      case 6: 
        verb = "attacked ";
        break;
      case 7: 
        verb = "excaped ";
        break;
      case 8: 
        verb = "swam ";
        break;
      case 9: 
        verb = "tore ";
        break; 
    }
    return verb; 
  }//end of verb method 
  public static String objectNoun()
  {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    String oNoun = " "; 
  
  
    switch(randomInt)
    {
      case 0: 
      oNoun = "spider ";
        break; 
      case 1: 
       oNoun = "elephent "; 
        break; 
      case 2: 
       oNoun = "meal ";
        break; 
      case 3: 
        oNoun = "hunter ";
        break; 
      case 4: 
        oNoun = "witch ";
        break;
      case 5: 
        oNoun = "sweatshirt ";
        break;
      case 6: 
        oNoun = "bottle ";
        break;
      case 7: 
        oNoun = "pen ";
        break;
      case 8: 
        oNoun = "fireball ";
        break;
      case 9: 
        oNoun = "lamp ";
        break; 
    }
    return oNoun; 
  }//end of objective noun method 
}//end of the class
  
  
  
  