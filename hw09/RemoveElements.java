// CSE 002 hw09 #2
// George Huang
// 11/27/2018
// This program 
import java.util.*;
public class RemoveElements{
  public static int[] randomInput() 
  {
    int[] a = new int[10];
    for(int i = 0; i < 10; i++) 
    {
      a[i] = (int)(Math.random() * 10);
    }
    return a;
  }
  
  
  public static int[] delete(int[] a, int pos) 
  {
    int[] b = new int[a.length - 1]; //since we are deleting one position
    for(int i = 0; i < pos;i++) 
    {
      b[i] = a[i];
    }
    for(int i = pos; i < 9;i++) 
    {
      b[i] = a[i + 1]; // skipping the one at pos 
    }
    return b;
  }
  
  public static int[] remove(int[] a, int val)
  {
    /* I wrote two versions. The first one is clearly easilier, but initially my thought process is the second version(code i'll write for the exam)
    boolean notFinished = true;
    while(notFinished){
      for(int i = 0; i < a.length; i++){
        if(a[i] == val)
        {
          a = delete(a, i); 
          break;
        }
        if(i = a.length - 1){
          notFinished = false;
        }
      }
    }
    */
    int [] copy = a; 
    
    int counter = 0; 
    for(int i = 0; i < a.length; i++)
    {
      if(a[i] == val)
      {
        copy[i] = 0; 
        counter++;
      }
    }
    int [] result = new int [10 - counter]; 
    int resultIndex = 0;
    for(int i = 0; i < a.length; i++)
    {
      if(copy[i] != 0)
      {
        result[resultIndex] = copy[i]; 
        resultIndex ++;
      }
    }
   
      
    return result; 
  }
  
  public static void main(String[] args)
  {
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    do
    {
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
      
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
      
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
      
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
  }
  
  public static String listArray(int num[])
  {
    String out="{";
    for(int j=0;j<num.length;j++)
    {
      if(j>0)
      {
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
}// end of class

