// CSE 002 lab05
// George Huang
// 10/04/2018
// This program will ask the user for ask for 
// the course number, department name, the number of times it meets in a week, the time the class starts, 
// the instructor name, and the number of students
import java.util.*;
public class WhilePractice{
  public static void main(String arg[]){
    //Varibales 
    int courseNumber = 0; 
    String departmentName = " "; 
    int timesMeet = 0; 
    double timeStart = 0.00; 
    String instructorName = " "; 
    int studentsNumber = 0; 

    
    Scanner myScan = new Scanner(System.in); 
    
    System.out.println("Please enter the course number: "); 
    while(!myScan.hasNextInt())
    {
      myScan.next(); 
      System.out.println("Error, Please enter the course number as an integer: ");
    }
    courseNumber = myScan.nextInt();
    //______________________________________________________________________
    System.out.println("Please enter the department name: "); 
    while(myScan.hasNextInt() || myScan.hasNextDouble())
    {
      myScan.next(); 
      System.out.println("Error, Please enter the course number as an String: ");
    }
    departmentName = myScan.next(); 
    //______________________________________________________________________
    System.out.println("Please enter number of times it meets in a week: "); 
    while(!myScan.hasNextInt())
    {
      myScan.next(); 
      System.out.println("Error, Please enter the number of times it meets in a week as an integer: ");
    }
    timesMeet = myScan.nextInt(); 
    //______________________________________________________________________
    System.out.println("Please enter the time the class starts: (Please enter double)"); 
    while(myScan.hasNextInt()) //(myScan.hasNextInt() || myScan.hasNextLine()) does not work so I deleted myScan.hasNextLine() 
    {
      myScan.next(); 
      System.out.println("Error, Please enter the time class starts in double ex:7:30 is 7.30: ");
    }
    timeStart = myScan.nextDouble(); 
    //______________________________________________________________________
    System.out.println("Please enter the instructor name: "); 
    while(myScan.hasNextInt() || myScan.hasNextDouble())
    {
      myScan.next(); 
      System.out.println("Error, Please enter the instructor name as an String: ");
    }
    instructorName = myScan.next();
    //______________________________________________________________________
    System.out.println("Please enter the number of students: "); 
    while(!myScan.hasNextInt())
    {
      myScan.next(); 
      System.out.println("Error, Please enter the number of students as an integer: ");
    }
    studentsNumber = myScan.nextInt(); 
    
    //Summary 
    System.out.println("Course Number: " + courseNumber + ", Department Name: " + departmentName + ", Times meet per week: " + timesMeet
    + ", Class Start Time: " + timeStart + ", Instructor Name: " + instructorName + ", Number of Students: " + studentsNumber + ". "); 
    
  }
}