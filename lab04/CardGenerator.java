// CSE 002 Lab04
// George Huang
// 09/20/2018
// This program will pick a random card and print out the name of the card
public class CardGenerator{
  public static void main(String arg[]){
    
    int cardNumber = (int)(Math.random()*51)+1;
    String suitName = " "; 
    String cardName = " ";
    if(cardNumber > 0 && cardNumber < 14){
      suitName = "Diamonds";
    }
    if(cardNumber > 13 && cardNumber < 27){
      suitName = "Clubs";
      cardNumber -= 13; 
    }
    if(cardNumber > 26 && cardNumber < 40){
      suitName = "Hearts";
      cardNumber -= 26;
    }
    if(cardNumber > 39 && cardNumber < 53){
      suitName = "Spades";
      cardNumber -= 39; 
    }
    System.out.println(cardNumber); 
    switch(cardNumber){
      case 1: cardName = "Ace of "; 
        break; 
      case 2: cardName = "2 of "; 
        break; 
      case 3: cardName = "3 of "; 
        break;
      case 4: cardName = "4 of "; 
        break;
      case 5: cardName = "5 of "; 
        break;
      case 6: cardName = "6 of "; 
        break;
      case 7: cardName = "7 of "; 
        break;
      case 8: cardName = "8 of "; 
        break;
      case 9: cardName = "9 of "; 
        break;
      case 10: cardName = "10 of "; 
        break;
      case 11: cardName = "Jack of "; 
        break;
      case 12: cardName = "Queen of "; 
        break;
      case 13: cardName = "King of "; 
        break;
    }
    
    System.out.println("You picked the " + cardName + suitName); 
    
    
    
    
    
    
  }
}
