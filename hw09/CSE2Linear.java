// CSE 002 hw09
// George Huang
// 11/26/2018
// This program 
import java.util.*;
public class CSE2Linear{
  public static void main(String arg[]){
    Scanner myScan = new Scanner(System.in); 
    boolean number = true; 
    boolean firstTime = true; 
    int userInput = 0;
    int index = 0; 
    //Question: how to check the range and the int together????
    int size = 15;
    int [] grades = new int [size];     
      while(number)
      {
        System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
        if(!myScan.hasNextInt())
        {
          number = true; 
          myScan.next(); 
          System.out.println("Error, Please enter an integer: ");
        }
        else if((userInput = myScan.nextInt()) < 0 || userInput > 100)
        {
          number = true; 
          System.out.println("This integer is out of range: enter from 0-100"); 
        }
        else if(index > 0)
        {
          if(userInput < grades[index - 1])
          {
            number = true; 
            System.out.println("This integer should bigger than or equal to the last integer");
          }
          else
          {
            grades[index] = userInput; 
            index++; 
            if(index == size)
            {
              number = false; 
            }
          }
        }
        else if(firstTime)
        {
          grades[index] = userInput; 
          index++; 
          firstTime = false; 
        }
      }//end of determining the user input. 
    
    for(int i = 0; i < size; i++) //print out the ascending array 
    {
      System.out.print(grades[i] + " "); 
    }
    System.out.println(); 
    number = true; 
    
    //check the search input 
    int checkGrade = 0; 
    while(number) 
    {
      System.out.print("Enter a grade to search for: ");
      if(!myScan.hasNextInt())
      {
        number = true;
        String trash = myScan.next();
        System.out.println("This is not a integer.");
      }
      else
      {
        checkGrade = myScan.nextInt();
        number = false;
      }
    }
    binarySearch(grades, checkGrade);//figure binary search
    
    int[] newArray = scrambling(grades);
    System.out.println("Scrambled: ");
        for(int i = 0; i < size; i++) 
        {
          System.out.print(newArray[i] + " ");
        }
        System.out.println();
    
    //seach again with linear search
    number = true; 
    while(number) 
    {
      System.out.print("Enter a grade to search for: ");
      if(!myScan.hasNextInt())
      {
        number = true;
        String trash = myScan.next();
        System.out.println("This is not a integer.");
      }
      else
      {
        checkGrade = myScan.nextInt();
        number = false;
      }
    }
    linearSearch(newArray,checkGrade); 
    
    
    

   
  }//end of main method
  public static void binarySearch(int[] a, int target)
  {
    int counter = 0; 
    int high = a.length - 1;
    int low = 0;
    int iterations = 0;
    boolean determine = false;
    while (high >= low) 
    {
      int mid = (high + low) / 2;
      if (target > a[mid]) 
      {
        low = mid + 1;
      }
      else if (target < a[mid]) 
      {
        high = mid - 1;
      }
      else if (target == a[mid])
      {               
        iterations++;
        determine = true; 
        break; 
      } 
      iterations++; 
    }
      if(determine) 
      {
        System.out.println(target + " was found with " + iterations + " iterations");
      }
      else
      {
        System.out.println(target + " was not found with " + iterations + " iterations");
      }
  }
  public static int[] scrambling(int[] a) //Scramble the array by give them the random position
  {
        int arrayLength = a.length;
        int[] newArray = new int[arrayLength];
        for(int i = 0; i < arrayLength; i++) 
        {
          boolean notFinish = true;
          while(notFinish) 
          {
            int position = (int)(Math.random() * arrayLength);
            if(a[position] != 0) 
            {
              newArray[i] = a[position];
              a[position] = 0; 
              notFinish = false; 
            }
          }
        }
    return newArray;
  }
  public static void linearSearch(int[] a, int target) 
  {
    int i = 0;
    boolean determine = false;
    int iterations = 0;
    for(i = 0; i < a.length; i++) 
    {
      if(a[i] == target) 
      {
        iterations = i; 
        determine = true; 
        break;
      }
    }

    if(determine) 
    {
      System.out.println(target + " was found with " + iterations + " iterations");
    }
    else 
    {
      System.out.println(target + " was not found with 15 iterations");
    }
   }
}//end of class
